package rs.tfzr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaurinaAplikacijaApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaurinaAplikacijaApplication.class, args);
	}

}
