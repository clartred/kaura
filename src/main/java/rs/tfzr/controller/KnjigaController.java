package rs.tfzr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.tfzr.model.Knjiga;
import rs.tfzr.model.Korisnik;
import rs.tfzr.service.KnjigaService;

import java.util.List;

@RestController
@RequestMapping("/knjiga")
public class KnjigaController {

    private KnjigaService knjigaService;

    @Autowired
    public KnjigaController(KnjigaService knjigaService) {
        this.knjigaService = knjigaService;
    }

    @GetMapping
    public List<Knjiga> getAll() {
        return knjigaService.getAll();
    }

    @GetMapping("/{id}")
    public Knjiga getOne(@PathVariable("id") Long id) {
        return knjigaService.getOne(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        knjigaService.delete(id);
    }

    @PostMapping
    public Knjiga save(@RequestBody Knjiga knjiga) {
        return knjigaService.save(knjiga);
    }
}
