package rs.tfzr.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.tfzr.model.Knjiga;
import rs.tfzr.model.Korisnik;
import rs.tfzr.service.KorisnikService;

import java.util.List;

@RestController
@RequestMapping("/korisnik")
public class KorisnikController {

    private KorisnikService korisnikService;

    @Autowired
    public KorisnikController(KorisnikService korisnikService) {
        this.korisnikService = korisnikService;
    }

    @GetMapping
    public List<Korisnik> getAll() {
        return korisnikService.getAll();
    }

    @GetMapping("/{id}")
    public Korisnik getOne(@PathVariable("id") Long id) {
        return korisnikService.getOne(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        korisnikService.delete(id);
    }

    @PostMapping
    public Korisnik save(@RequestBody Korisnik korisnik) {
        return korisnikService.save(korisnik);
    }

    @PostMapping("/nova-knjiga/{korisnik-id}")
    public Korisnik saveBook(@RequestBody Knjiga knjiga, @PathVariable("korisnik-id") Long id){
        return korisnikService.saveBook(knjiga, id);
    }


}
