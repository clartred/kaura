package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Knjiga;
import rs.tfzr.model.Korisnik;
import rs.tfzr.repository.KnjigaRepository;

import java.util.List;

@Service
public class KnjigaService {

    private KnjigaRepository knjigaRepository;

    @Autowired
    public KnjigaService(KnjigaRepository knjigaRepository) {
        this.knjigaRepository = knjigaRepository;
    }

    public List<Knjiga> getAll() {
        return knjigaRepository.findAll();
    }

    public Knjiga getOne(Long id) {
        return knjigaRepository.getOne(id);
    }

    public Knjiga save(Knjiga knjiga) {
        return knjigaRepository.save(knjiga);
    }

    public void delete(Long id) {
        knjigaRepository.deleteById(id);
    }


}
