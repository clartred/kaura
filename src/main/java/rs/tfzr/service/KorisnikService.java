package rs.tfzr.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.tfzr.model.Knjiga;
import rs.tfzr.model.Korisnik;
import rs.tfzr.repository.KorisnikRepository;

import java.util.List;

@Service
public class KorisnikService {

    private KorisnikRepository korisnikRepository;

    @Autowired
    public KorisnikService(KorisnikRepository korisnikRepository) {
        this.korisnikRepository = korisnikRepository;
    }

    public List<Korisnik> getAll() {
        return korisnikRepository.findAll();
    }

    public Korisnik getOne(Long id) {
        return korisnikRepository.getOne(id);
    }

    public Korisnik save(Korisnik korisnik) {
        return korisnikRepository.save(korisnik);
    }

    public void delete(Long id) {
        korisnikRepository.deleteById(id);
    }

    public Korisnik saveBook(Knjiga knjiga, Long userId) {
        Korisnik korisnik = korisnikRepository.getOne(userId);
        korisnik.getKnjige().add(knjiga);
        korisnikRepository.save(korisnik);
        return korisnik;
    }
}
